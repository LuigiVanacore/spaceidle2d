extends Panel

var money : float = 5
var store_count : float = 0

var store_cost : float = 5
var store_profit : float = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	update_UI()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_UI()


func update_UI():
	update_moneyLabel()
	update_storeCountLabel()

func update_moneyLabel():
	$MoneyLabel.text = "Money: $" + str(money)

func update_storeCountLabel():
	$StoreCountLabel.visible = true
	$StoreCountLabel.text = str(store_count)




func _on_BuyButton_pressed():
	if store_cost <= money:
		money -= store_cost
		store_count += 1
	else:
		$PopupDialog.popup()
		$StoreCountLabel.visible = false


func _on_ClickButton_pressed():
	$StoreTimer.start()
	$ProgressTimer.start()


func _on_StoreTimer_timeout():
	$StoreTimer.stop()
	$ProgressTimer.stop()
	$ProgressBar.set("value", 0)
	money += store_profit * store_count

func _on_ProgressTimer_timeout():
	var  current_progress = ( $StoreTimer.wait_time - $StoreTimer.time_left ) / $StoreTimer.wait_time
	$ProgressBar.set("value", current_progress)
