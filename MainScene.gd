extends Control

signal MINE_PURCHASED


var money : float = 100.0
var miners : Dictionary

# Called when the node enters the scene tree for the first time.
func _ready():
	$LabelsMenu/MoneyLabel.text = "$ " + str(money)
	
func _process(delta):
	Update_UI()
	

func Update_UI():
	$LabelsMenu.get_node("MoneyLabel").text = "Money: $ " + str(money)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func get_mine_quantity(mine_name : String ) -> float :
	return miners[mine_name]
	

func _on_MinersMenuButton_pressed():
	$MinersManager.show()


func _on_ForgeMenuButton_pressed():
	$MinersManager.hide()


func _on_CrafterMenuButton_pressed():
	pass # Replace with function body.


func _on_MarketMenuButton_pressed():
	pass # Replace with function body.


func _on_UpgradeMenuButton_pressed():
	pass # Replace with function body.


func _on_OptionMenuButton_pressed():
	pass # Replace with function body.
	
	
func On_Mining_Purchased(_self, amt):
	if amt <= money:
		money -= amt
		emit_signal("MINE_PURCHASED", _self)
		
func MiningCompleted(quantity_mined : float, mine_name : String ) :
	miners[mine_name] += quantity_mined
