 extends Object

# The default values
var default_data = {
	"player" : {
		"name" : "Jamie",
		"level" : 3,
		"health" : 10
		},
	"options" : {
		"music_volume" : 0.5,
		"cheat_mode" : false
		},
	"levels_completed" : [1, 2, 3]
}

var data = { }




func load_game(path):
	var file = File.new()
	var mines : Array
	if not file.file_exists(path):
		return
	file.open(path, file.READ)
	var text = file.get_as_text()
	file.close()
	data = JSON.parse(text)
	if data.error != OK:
		print("Json error")
		return

