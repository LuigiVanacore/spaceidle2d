extends Panel


var mine_name : String
var level : int = 0
var total_quantity : float = 0.0
var quantity_x_level : float = 1.0
var upgrade_cost : float = 5.0
var total_upgrade_cost : float = upgrade_cost
var root : Node
var timer_running : bool = false


signal MINING_FINISHED
signal ON_MINING_PURCHASE


# Called when the node enters the scene tree for the first time.
func _ready():
	$CostUpgradeLabel.text = "Cost: " + str(upgrade_cost)
	$QuantityLabel.text = "Qt.: " + str(total_quantity)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_ui()
	if !timer_running and level > 0:
		timer_running = true
		$ProductionTimer.start()
		$ProgressTimer.start()
	
func set_name(name : String):
	$NameLabel.text = name

func update_ui():
	total_quantity = root.get_mine_quantity(mine_name)
	if level == 0:
		$UpgradeButton.text = "Start Mining!"
	else:
		$UpgradeButton.text = "Upgrade"
	$LevelLabel.text = "Lv. " + str(level)
	$CostUpgradeLabel.text = "Cost: " + str(total_upgrade_cost)
	$QuantityLabel.text = "Qt.: " + str(total_quantity)



func mine_purchased(_mine):
	if self == _mine:
		level += 1
		total_upgrade_cost += upgrade_cost
	
	
func _on_ProductionTimer_timeout():
	timer_running = false
	$ProductionTimer.stop()
	$ProgressTimer.stop()
	$ProgressBar.set("value",0)
	emit_signal("MINING_FINISHED", quantity_x_level * level, mine_name)
	

func _on_ProgressTimer_timeout():
	var current_progress = ( $ProductionTimer.wait_time - $ProductionTimer.time_left ) / $ProductionTimer.wait_time
	$ProgressBar.set("value", current_progress)



func _on_UpgradeButton_pressed():
	emit_signal("ON_MINING_PURCHASE",self,total_upgrade_cost)
