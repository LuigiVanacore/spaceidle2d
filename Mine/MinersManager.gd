# Made by: Luigi Vanacore
# Script for MinerManager Scene
# This Script manage the creation and the management of 
# the Miners Scenes. It creates a pool of Miners object
# from a Json file and manage them connecting to 
# MainScene script.
#
extends ScrollContainer

#path for the Json for creating the Miners
var path = "user://mines.json"

#Scene for Base Miner
var base_mine = preload("res://Mine/BaseMine.tscn")


var mines : Dictionary
var data = { }
var root : Object

func _ready():
	root = get_parent()
	load_data()


func onMiningFinished(_self, quantity):
	var mine_name = _self.mine_name
	mines[mine_name] = quantity



func load_data():
	var file = File.new()
	if not file.file_exists(path):
		return
	file.open(path, file.READ)
	var text = file.get_as_text()
	file.close()
	data = JSON.parse(text)
	if data.error != OK:
		print("Json error")
		return
	var base_mine_instance
	for mine in data.result.values():
		var name : String = mine.name
		base_mine_instance = base_mine.instance()
		base_mine_instance.get_node("NameLabel").text = name + " mine"
		base_mine_instance.mine_name = name
		root.miners[name] = 0.0
		base_mine_instance.root = root
		root.connect("MINE_PURCHASED",base_mine_instance, "mine_purchased")
		base_mine_instance.connect("ON_MINING_PURCHASE", root,"On_Mining_Purchased")
		base_mine_instance.connect("MINING_FINISHED", root, "MiningCompleted")
		$VBoxContainer.add_child(base_mine_instance)
		mines[name] = 0
	#Add a Control node so the slide will work
	$VBoxContainer.add_child(Control.new())


